+++
title    = "branko juran"
+++

Office 04.4.03, `bj(at)math.ku.dk`

_Research Interest: Equivariant stable homotopy theory_

***

####  Prepublications 

[[1]] [Orbifolds, Orbispaces and Global Homotopy Theory][1], preprint, 2020.

[1]:  https://arxiv.org/abs/2006.12374
